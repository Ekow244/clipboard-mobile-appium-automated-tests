import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import java.net.MalformedURLException;
import java.net.URL;

public class SampleTest{
    public AndroidDriver driver;


    @BeforeTest
    public void setUp() throws MalformedURLException{
        String appiumServerURL ="http://127.0.0.1:4723/wd/hub";
        DesiredCapabilities dc = new DesiredCapabilities();
        dc.setCapability("platformName", "Android");
        dc.setCapability("platformVersion", "8.1 ");
        dc.setCapability("app","/home/dev2/Downloads/app-prod-debug (8).apk");
        dc.setCapability("deviceName", "040291495L002891");
        dc.setCapability("automationName", "Appium");
        driver = new AndroidDriver(new URL(appiumServerURL),dc);


    }

    @Test
    public void firstTest() throws InterruptedException {
        TouchAction touchAction = new TouchAction(driver);
        Thread.sleep(10000);
        driver.findElementByClassName("android.widget.Button").click();
        Thread.sleep(18000);
        driver.findElementById("email").sendKeys("ekow.melfah@gmail.com");

        driver.findElementById("password").sendKeys("0v3r10rD");

        Thread.sleep(8000);

        driver.findElementByAndroidUIAutomator
                ("new UiScrollable(new UiSelector()" +
                        ".scrollable(true).instance(0))" +
                        ".scrollIntoView(new UiSelector()" +
                        ".textContains(\"Submit\").instance(0))")
                .click();

        Thread.sleep(10000);
        driver.findElementByClassName("android.widget.Button").click();

        driver.findElementByClassName("android.widget.Button").click();

        Thread.sleep(5000);
        driver.findElementByClassName("android.widget.Button").click();

        Thread.sleep(5000);
        driver.findElement(By.xpath("/hierarchy/android.widget" +
                ".FrameLayout/android" +
                ".widget.LinearLayout/android" +
                ".widget.FrameLayout/android.widget" +
                ".LinearLayout/android.widget" +
                ".FrameLayout/android.widget" +
                ".FrameLayout/android.view" +
                ".ViewGroup/android.widget" +
                ".ScrollView/android.view" +
                ".ViewGroup/android.view" +
                ".ViewGroup[2]/android.view" +
                ".ViewGroup/android.view" +
                ".ViewGroup/android.widget" +
                ".TextView\n"))
                .click();


    }

    @Test
    public void loginWithoutEnteringCredentials() throws InterruptedException {
        Thread.sleep(10000);
        driver.findElementByClassName("android.widget.Button").click();


        Thread.sleep(18000);
        driver.findElementByClassName("android.widget.Button").click();

        Thread.sleep(30000);
        driver.findElement(By.xpath("/hierarchy/android.widget" +
                ".FrameLayout/android" +
                ".widget.LinearLayout/android" +
                ".widget.FrameLayout/android.widget" +
                ".LinearLayout/android.widget" +
                ".FrameLayout/android.widget" +
                ".FrameLayout/android.view" +
                ".ViewGroup/android.widget" +
                ".ScrollView/android.view" +
                ".ViewGroup/android.view" +
                ".ViewGroup[2]/android.view" +
                ".ViewGroup/android.view" +
                ".ViewGroup/android.widget" +
                ".TextView\n"))
                .click();

        driver.findElement(By.xpath("/hierarchy/android.widget" +
                ".FrameLayout/android.widget" +
                ".LinearLayout/android.widget" +
                ".FrameLayout/android.widget" +
                ".LinearLayout/android.widget" +
                ".FrameLayout/android.widget" +
                ".FrameLayout/android.view" +
                ".ViewGroup/android.view" +
                ".ViewGroup/android" +
                ".widget.Button/android" +
                ".widget.TextView\n"))
                .click();

    }
    @Test
    public void selectAnOrg() throws InterruptedException {
        Thread.sleep(10000);
        driver.findElementByClassName("android.widget.Button").click();


        Thread.sleep(18000);
        driver.findElementByClassName("android.widget.Button").click();

        TouchAction touchAction = new TouchAction(driver);
        touchAction.tap(PointOption.point(237, 747)).perform();




    }

    @Test
    public void flowInteraction() throws InterruptedException {
        TouchAction touchAction = new TouchAction(driver);
        Thread.sleep(10000);
        driver.findElementByClassName("android.widget.Button").click();


        Thread.sleep(18000);
        driver.findElementByClassName("android.widget.Button").click();

        touchAction.tap(PointOption.point(237, 747)).perform();


        touchAction.tap(PointOption.point(222, 438)).perform();
        Thread.sleep(20000);

        touchAction.tap(PointOption.point(209, 432)).perform(); //will tree


        Thread.sleep(5000);
        touchAction.tap(PointOption.point(251, 678)).perform(); //next


        Thread.sleep(5000);
        touchAction.tap(PointOption.point(206, 251)).perform();

        Thread.sleep(5000);
        touchAction.tap(PointOption.point(251, 678)).perform();

        //result of rapid test
        touchAction.tap(PointOption.point(145, 250)).perform();

        touchAction.tap(PointOption.point(251, 678)).perform(); //next


        //msmcq
        Thread.sleep(10000);
        touchAction.tap(PointOption.point(182, 150)).perform();
        touchAction.tap(PointOption.point(171, 303)).perform();

        touchAction.tap(PointOption.point(251, 678)).perform(); //next





    }

}
